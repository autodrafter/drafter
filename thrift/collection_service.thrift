include "objects.thrift"

service Collector {

  list<objects.NbaPerformance> get_nba_performances(1:list<string> athlete_names, 2:i32 num_performances);

  bool ping();

}
