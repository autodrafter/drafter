enum League {
  NBA,
  NFL,
  NHL,
  NASCAR,
  SOCCER,
  GOLF,
  CFL,
  LOL,
  UNKNOWN
}

enum ContestSite {
  DRAFT_KINGS
}

enum ThriftTransport {
  BUFFERED,
}

enum ThriftProtocol {
  BINARY,
}

enum ScoreType {
  CLASSIC,
  SHOWDOWN,
  PICK_EM,
  LATE_SWAP
}

struct Athlete {
  1: string full_name;
  2: League league; 
  3: list<i32> positions;
}

struct ActiveAthlete {
  1: Athlete athlete;
  2: double cost;
  3: double predicted_points;
}

struct StartingAthlete {
  1: ActiveAthlete athlete;
  2: i32 position;
}

struct NbaGameStats {
  1: double points;
  2: double assists;
  3: double steals;
  4: double blocks;
  5: double three_pointers;
  6: double rebounds;
  7: double turnovers;
}

struct NbaPerformance {
  1: Athlete athlete;
  2: NbaGameStats stats;
}

struct ThriftServiceConfig {
  1: string host_name;
  2: i32 host_port;
  3: ThriftTransport transport;
  4: ThriftProtocol protocol;
}

struct Lineup {
  1: list<StartingAthlete> athletes; 
  2: double score;
}

struct DraftKingSpec {
  1: i64 contest_id;
  2: i64 draft_group_id;
}

struct ContestSpec {
  1: double salary_cap;
  2: i32 num_positions;
  3: string name;
  4: string start_time;
  5: double entry_fee;
  6: double total_payout;
  7: i32 total_entries;
  8: i32 max_entries;
  9: ContestSite site;
  10: DraftKingSpec dk_spec;
  11: League league;
  12: ScoreType score_type;
}
