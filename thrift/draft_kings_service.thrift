include "objects.thrift"

service DraftKings {
  list<objects.ContestSpec> get_contests(1:objects.League league);

  list<objects.ActiveAthlete> get_athletes_for_contest(1:objects.ContestSpec contest);

  bool ping();
}
