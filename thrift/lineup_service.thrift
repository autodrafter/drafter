include "objects.thrift"

service LineupGenerator {
  list<objects.Lineup> get_top_lineups(1:list<objects.ActiveAthlete> athletes,
                                       2:objects.ContestSpec contest, 3:i32 num_results);

  bool ping();
}
