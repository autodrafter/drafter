include "objects.thrift"

service Predictor {
  list<objects.ActiveAthlete> predict_performances(1:list<objects.Athlete> atheletes,
                                                   2:objects.ScoreType score_type);

  bool ping();
}
