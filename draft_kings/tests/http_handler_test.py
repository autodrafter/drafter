# pylint: disable=missing-docstring
"""
Tests for http draft kings handler.
"""

import unittest
import mock
from parameterized import parameterized


import test_data
from draft_kings import http_handler
from objects import ttypes as objects


class TestHttpDraftKingsHandler(unittest.TestCase):
  """Tests for HttpHandler."""

  def test_simple_construction(self):
    self.assertIsNotNone(http_handler.HttpHandler())

  def test_ping(self):
    self.assertTrue(http_handler.HttpHandler().ping())

  @parameterized.expand([
      (objects.League.NBA, 5),
      (objects.League.LOL, 1),
      (objects.League.NHL, 1)])
  @mock.patch('draft_kings.http_handler._get_contest_json_for_league')
  def test_get_contests_count(self, league, count, mock_get_json):
    mock_get_json.return_value = test_data.SAMPLE_GET_CONTEST_STRING

    result = http_handler.HttpHandler().get_contests(league)

    mock_get_json.assert_called_once_with(league)

    self.assertEqual(count, len(result))
    for spec in result:
      self.assertEqual(league, spec.league)

  @mock.patch('draft_kings.http_handler._get_contest_json_for_league')
  def test_get_contests_parse(self, mock_get_json):
    mock_get_json.return_value = test_data.SAMPLE_GET_CONTEST_STRING
    expected_value = objects.ContestSpec(
        salary_cap=50000,
        num_positions=8,
        name='NHL $17K Stick Lift',
        start_time='2018-03-05 00:00:00+00:00',
        entry_fee=275,
        total_payout=17000,
        total_entries=0,
        max_entries=68,
        site=objects.ContestSite.DRAFT_KINGS,
        dk_spec=objects.DraftKingSpec(contest_id=54108468, draft_group_id=17886),
        league=objects.League.NHL,
        score_type=objects.ScoreType.CLASSIC

    )

    result = http_handler.HttpHandler().get_contests(objects.League.NHL)

    mock_get_json.assert_called_once_with(objects.League.NHL)
    self.assertEqual(1, len(result))
    self.assertEqual(expected_value, result[0])

  @mock.patch('draft_kings.http_handler._get_contest_json_for_league')
  def test_full_request_parses(self, mock_get_json):
    mock_get_json.return_value = test_data.get_full_contests()

    result = http_handler.HttpHandler().get_contests(objects.League.NHL)

    self.assertIsNotNone(result)

  @mock.patch('draft_kings.http_handler._get_player_json_for_contest')
  def test_get_players_parse(self, mock_get_json):
    mock_get_json.return_value = test_data.SAMPLE_DRAFTABLES
    expected_values = [
        objects.ActiveAthlete(objects.Athlete('James Harden', objects.League.NBA, [0, 1, 5, 7]),
                              11700, 0.0),
        objects.ActiveAthlete(objects.Athlete('Cameron Payne', objects.League.NBA, [2, 3, 4, 6, 7]),
                              5500, 0.0),
    ]
    contest = objects.ContestSpec(league=objects.League.NBA)

    result = http_handler.HttpHandler().get_athletes_for_contest(contest)

    mock_get_json.assert_called_once_with(contest)
    self.assertItemsEqual(expected_values, result)

  @mock.patch('draft_kings.http_handler._get_player_json_for_contest')
  def test_get_players_full_result(self, mock_get_json):
    mock_get_json.return_value = test_data.get_full_players()

    contest = objects.ContestSpec(league=objects.League.NBA)

    result = http_handler.HttpHandler().get_athletes_for_contest(contest)

    mock_get_json.assert_called_once_with(contest)
    self.assertIsNotNone(result)
