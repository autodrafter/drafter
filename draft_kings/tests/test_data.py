"""Has data for use with draft kings tests."""

FULL_CONTESTS_PATH = "draft_kings/tests/full_contests.json"
FULL_PLAYERS_PATH = "draft_kings/tests/full_players.json"


def get_full_contests():
  """Returns a full response of contests, good for overall smoke test of full options."""
  with open(FULL_CONTESTS_PATH, 'r') as request_file:
    return request_file.read()


def get_full_players():
  """Returns a full response of players,  good for overall smoke test of full options."""
  with open(FULL_PLAYERS_PATH, 'r') as request_file:
    return request_file.read()


SAMPLE_DRAFTABLES = u"""
{
    "competitions": [
        {
            "awayTeam": {
                "abbreviation": "CHI",
                "city": "Chicago",
                "teamId": 4,
                "teamName": "Bulls"
            },
            "competitionId": 5505818,
            "competitionState": "Upcoming",
            "depthChartsAvailable": true,
            "homeTeam": {
                "abbreviation": "DET",
                "city": "Detroit",
                "teamId": 8,
                "teamName": "Pistons"
            },
            "name": "CHI @ DET",
            "sport": "NBA",
            "startTime": "2018-03-24T23:00:00.0000000Z",
            "startingLineupsAvailable": false,
            "venue": "Little Caesars Arena"
        }
    ],
    "draftAlerts": [],
    "draftStats": [
        {
            "abbr": "FPPG",
            "id": 219,
            "name": "Fantasy Points Per Game",
            "order": 10
        },
        {
            "abbr": "OPRK",
            "id": -2,
            "name": "Opponent Rank",
            "order": 20
        }
    ],
    "draftables": [
        {
            "competition": {
                "competitionId": 5506674,
                "name": "NO @ HOU",
                "nameDisplay": [
                    {
                        "value": "NO"
                    },
                    {
                        "value": " @ "
                    },
                    {
                        "isEmphasized": true,
                        "value": "HOU"
                    }
                ],
                "startTime": "2018-03-25T00:00:00.0000000Z"
            },
            "displayName": "James Harden",
            "draftAlerts": [],
            "draftStatAttributes": [
                {
                    "id": 219,
                    "sortValue": "56.2",
                    "value": "56.2"
                },
                {
                    "id": -2,
                    "quality": "High",
                    "sortValue": "30",
                    "value": "30th"
                }
            ],
            "draftableId": 10408693,
            "firstName": "James",
            "isDisabled": false,
            "isSwappable": true,
            "lastName": "Harden",
            "newsStatus": "Recent",
            "playerAttributes": [],
            "playerGameAttributes": [],
            "playerGameHash": "395388-5506674",
            "playerId": 395388,
            "playerImage160": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_retina/395388.png",
            "playerImage50": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_50/395388.png",
            "playerImage65": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_65/395388.png",
            "playerImageFull": "https://d327rxwuxd0q0c.cloudfront.net/nba/players/395388.png",
            "position": "PG/SG",
            "rosterSlotId": 20,
            "salary": 11700,
            "shortName": "J. Harden",
            "status": "Q",
            "teamAbbreviation": "HOU",
            "teamId": 10
        },
        {
            "competition": {
                "competitionId": 5505818,
                "name": "CHI @ DET",
                "nameDisplay": [
                    {
                        "isEmphasized": true,
                        "value": "CHI"
                    },
                    {
                        "value": " @ "
                    },
                    {
                        "value": "DET"
                    }
                ],
                "startTime": "2018-03-24T23:00:00.0000000Z"
            },
            "displayName": "Cameron Payne",
            "draftAlerts": [],
            "draftStatAttributes": [
                {
                    "id": 219,
                    "sortValue": "20.9",
                    "value": "20.9"
                },
                {
                    "id": -2,
                    "quality": "Medium",
                    "sortValue": "16",
                    "value": "16th"
                }
            ],
            "draftableId": 10408827,
            "firstName": "Cameron",
            "isDisabled": false,
            "isSwappable": true,
            "lastName": "Payne",
            "newsStatus": "Breaking",
            "playerAttributes": [],
            "playerGameAttributes": [],
            "playerGameHash": "792338-5505818",
            "playerId": 792338,
            "playerImage160": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_retina/792338.png",
            "playerImage50": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_50/792338.png",
            "playerImage65": "https://d327rxwuxd0q0c.cloudfront.net/m/nba_65/792338.png",
            "playerImageFull": "https://d327rxwuxd0q0c.cloudfront.net/nba/players/792338.png",
            "position": "SF/PF/C",
            "rosterSlotId": 21,
            "salary": 5500,
            "shortName": "C. Payne",
            "status": "None",
            "teamAbbreviation": "CHI",
            "teamId": 4
        }
    ],
    "errorStatus": {},
    "playerGameAttributes": []
}
"""

SAMPLE_GET_CONTEST_STRING = u"""
{
    "Contests": [
        {
            "a": 8,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 0,
            "dg": 17874,
            "dgpo": 569426.3,
            "ec": 0,
            "fpp": 8,
            "fwt": false,
            "gameType": "Classic",
            "id": 54079270,
            "ir": 0,
            "isOwner": false,
            "m": 22058,
            "mec": 150,
            "n": "NBA $150K Late Night Snack [$25K to 1st] (Night)",
            "nt": 19289,
            "pd": {
                "Cash": "150000"
            },
            "po": 150000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 4,
            "sa": true,
            "sd": "/Date(1520132400000)/",
            "sdstring": "Sat 10:00PM",
            "so": -99999999,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 303495,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 27,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 1,
            "dg": 17874,
            "dgpo": 569426.3,
            "ec": 0,
            "fpp": 27,
            "fwt": false,
            "gameType": "Classic",
            "id": 54079269,
            "ir": 0,
            "isOwner": false,
            "m": 647,
            "mec": 19,
            "n": "NBA $15K Crossover (Night)",
            "nt": 463,
            "pd": {
                "Cash": "15000"
            },
            "po": 15000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 4,
            "sa": true,
            "sd": "/Date(1520132400000)/",
            "sdstring": "Sat 10:00PM",
            "so": -99999998,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 296356,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 1060,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 2,
            "dg": 17874,
            "dgpo": 569426.3,
            "ec": 0,
            "fpp": 1060,
            "fwt": false,
            "gameType": "Classic",
            "id": 54079268,
            "ir": 0,
            "isOwner": false,
            "m": 8,
            "mec": 1,
            "n": "NBA $8K All-Star [Single Entry] (Night)",
            "nt": 5,
            "pd": {
                "Cash": "8000"
            },
            "po": 8000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 4,
            "sa": true,
            "sd": "/Date(1520132400000)/",
            "sdstring": "Sat 10:00PM",
            "so": -99999997,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 279756,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 4,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 7,
            "dg": 17803,
            "dgpo": 1174754.6,
            "ec": 0,
            "fpp": 4,
            "fwt": false,
            "gameType": "Classic",
            "id": 54007058,
            "ir": 0,
            "isOwner": false,
            "m": 5945,
            "mec": 1,
            "n": "NAS $20K Brake Pad [Single Entry] (Cup)",
            "nt": 2412,
            "pd": {
                "Cash": "20000"
            },
            "po": 20000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 10,
            "sa": true,
            "sd": "/Date(1520195400000)/",
            "sdstring": "Sun 3:30PM",
            "so": -99999991,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 305858,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 8,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 9,
            "dg": 17878,
            "dgpo": 1743092.4,
            "ec": 0,
            "fpp": 8,
            "fwt": false,
            "gameType": "Classic",
            "id": 54107516,
            "ir": 0,
            "isOwner": false,
            "m": 66176,
            "mec": 150,
            "n": "NBA $450K Hundred Thousandaire Excellent 8\u2019s [$100K to 1st!]",
            "nt": 300,
            "pd": {
                "Cash": "450000"
            },
            "po": 450000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 4,
            "sa": true,
            "sd": "/Date(1520208000000)/",
            "sdstring": "Sun 7:00PM",
            "so": -99999989,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 306179,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 275,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 31,
            "dg": 17886,
            "dgpo": 244270.8,
            "ec": 0,
            "fpp": 275,
            "fwt": false,
            "gameType": "Classic",
            "id": 54108468,
            "ir": 0,
            "isOwner": false,
            "m": 68,
            "mec": 2,
            "n": "NHL $17K Stick Lift",
            "nt": 0,
            "pd": {
                "Cash": "17000"
            },
            "po": 17000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 3,
            "sa": true,
            "sd": "/Date(1520208000000)/",
            "sdstring": "Sun 7:00PM",
            "so": -99999967,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 304680,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 5,
            "attr": {
                "IsGuaranteed": "true",
                "IsQualifier": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 98,
            "dg": 17878,
            "dgpo": 1743092.4,
            "ec": 0,
            "fpp": 5,
            "fwt": false,
            "gameType": "Classic",
            "id": 54100339,
            "ir": 0,
            "isOwner": false,
            "m": 237,
            "mec": 7,
            "n": "NBA $3.5M Fantasy Golf Millionaire SUPERSatellite [50x]",
            "nt": 9,
            "pd": {
                "Ticket": "50 Tickets"
            },
            "po": 1000,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 4,
            "sa": true,
            "sd": "/Date(1520208000000)/",
            "sdstring": "Sun 7:00PM",
            "so": -99999899,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 306318,
            "uc": 0,
            "ulc": 0
        },
        {
            "a": 3,
            "attr": {
                "IsGuaranteed": "true",
                "IsStarred": "true"
            },
            "cs": 1,
            "cso": 199,
            "dg": 17865,
            "dgpo": 17781.8,
            "ec": 0,
            "fpp": 3,
            "fwt": false,
            "gameType": "Classic",
            "id": 54064723,
            "ir": 0,
            "isOwner": false,
            "m": 237,
            "mec": 7,
            "n": "LOL $600 Critical Strike (LCK-LPL)",
            "nt": 59,
            "pd": {
                "Cash": "600"
            },
            "po": 600,
            "pt": 1,
            "rl": false,
            "rlc": 0,
            "rll": 99999,
            "s": 11,
            "sa": true,
            "sd": "/Date(1520150400000)/",
            "sdstring": "Sun 3:00AM",
            "so": -99999798,
            "ssd": null,
            "startTimeType": 0,
            "tix": false,
            "tmpl": 268465,
            "uc": 0,
            "ulc": 0
        }
    ],
    "DepositTransaction": null,
    "DirectChallengeModal": null,
    "DraftGroups": [
        {
            "AllowUGC": true,
            "ContestStartTimeSuffix": " (Early)",
            "ContestStartTimeType": 1,
            "ContestTypeId": 18,
            "DraftGroupId": 17885,
            "DraftGroupSeriesId": 0,
            "DraftGroupTag": "",
            "GameCount": 4,
            "GameSetKey": "7F293B33422A566E9C3225DA9DFB4A6C",
            "GameType": null,
            "GameTypeId": 4,
            "Games": null,
            "SortOrder": 999,
            "Sport": "NHL",
            "SportSortOrder": 4,
            "StartDate": "2018-03-04T20:00:00.0000000Z",
            "StartDateEst": "2018-03-04T15:00:00.0000000"
        },
        {
            "AllowUGC": null,
            "ContestStartTimeSuffix": " (PGA)",
            "ContestStartTimeType": 0,
            "ContestTypeId": 29,
            "DraftGroupId": 17891,
            "DraftGroupSeriesId": 9,
            "DraftGroupTag": "Featured",
            "GameCount": 1,
            "GameSetKey": "FA85C5A020C290AE3D328F71A79A327D",
            "GameType": null,
            "GameTypeId": 6,
            "Games": null,
            "SortOrder": 1,
            "Sport": "GOLF",
            "SportSortOrder": 2,
            "StartDate": "2018-03-08T10:00:00.0000000Z",
            "StartDateEst": "2018-03-08T05:00:00.0000000"
        },
        {
            "AllowUGC": null,
            "ContestStartTimeSuffix": " (PGA)",
            "ContestStartTimeType": 0,
            "ContestTypeId": 29,
            "DraftGroupId": 17806,
            "DraftGroupSeriesId": 9,
            "DraftGroupTag": "Featured",
            "GameCount": 1,
            "GameSetKey": "C21E484C4CAC884B1FDA39650C6BF0E8",
            "GameType": null,
            "GameTypeId": 6,
            "Games": null,
            "SortOrder": 999,
            "Sport": "GOLF",
            "SportSortOrder": 2,
            "StartDate": "2018-04-05T10:00:00.0000000Z",
            "StartDateEst": "2018-04-05T06:00:00.0000000"
        }
    ],
    "GameSets": [
        {
            "Competitions": [
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 53563,
                    "AwayTeamName": "Brighton and Hove Albion",
                    "AwayTeamScore": 0,
                    "Description": "EVE vs BHA",
                    "ExceptionalMessages": [],
                    "FullDescription": "Everton vs Brighton and Hove Albion",
                    "GameId": 5497362,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 40815,
                    "HomeTeamName": "Everton",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T15:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                },
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 40825,
                    "AwayTeamName": "Swansea City",
                    "AwayTeamScore": 0,
                    "Description": "HUD vs SWA",
                    "ExceptionalMessages": [],
                    "FullDescription": "Huddersfield Town vs Swansea City",
                    "GameId": 5497363,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 53565,
                    "HomeTeamName": "Huddersfield Town",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T15:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                },
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 40818,
                    "AwayTeamName": "Southampton",
                    "AwayTeamScore": 0,
                    "Description": "NEW vs SOU",
                    "ExceptionalMessages": [],
                    "FullDescription": "Newcastle United vs Southampton",
                    "GameId": 5497365,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 40812,
                    "HomeTeamName": "Newcastle United",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T15:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                },
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 40816,
                    "AwayTeamName": "Leicester City",
                    "AwayTeamScore": 0,
                    "Description": "WBA vs LEI",
                    "ExceptionalMessages": [],
                    "FullDescription": "West Bromwich Albion vs Leicester City",
                    "GameId": 5497367,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 40821,
                    "HomeTeamName": "West Bromwich Albion",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T15:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                },
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 43296,
                    "AwayTeamName": "Burnley",
                    "AwayTeamScore": 0,
                    "Description": "WHU vs BUR",
                    "ExceptionalMessages": [],
                    "FullDescription": "West Ham United vs Burnley",
                    "GameId": 5497368,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 40819,
                    "HomeTeamName": "West Ham United",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T15:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                },
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": 40820,
                    "AwayTeamName": "Crystal Palace",
                    "AwayTeamScore": 0,
                    "Description": "CHE vs CRY",
                    "ExceptionalMessages": [],
                    "FullDescription": "Chelsea vs Crystal Palace",
                    "GameId": 5497361,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": 40551,
                    "HomeTeamName": "Chelsea",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "SOC",
                    "StartDate": "2018-03-10T17:30:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                }
            ],
            "ContestStartTimeSuffix": " (EPL)",
            "GameSetKey": "51B3CF267480FB4717D32FEAE55CFA75",
            "GameStyles": [
                {
                    "Abbreviation": "CLA",
                    "Description": "Create an 8-player lineup while staying under the $50,000 salary cap",
                    "GameStyleId": 17,
                    "IsEnabled": true,
                    "Name": "Classic",
                    "SortOrder": 1,
                    "SportId": 12
                }
            ],
            "MinStartTime": "/Date(1520694000000)/",
            "SortOrder": null,
            "Tag": "Featured"
        },
        {
            "Competitions": [
                {
                    "AwayTeamCity": "",
                    "AwayTeamCompetitionCount": 1,
                    "AwayTeamCompetitionOrdinal": 1,
                    "AwayTeamId": -5,
                    "AwayTeamName": "Golf",
                    "AwayTeamScore": 0,
                    "Description": "Masters Tournament",
                    "ExceptionalMessages": [],
                    "FullDescription": "Masters Tournament",
                    "GameId": 5510798,
                    "HomeTeamCity": "",
                    "HomeTeamCompetitionCount": 1,
                    "HomeTeamCompetitionOrdinal": 1,
                    "HomeTeamId": -5,
                    "HomeTeamName": "Golf",
                    "HomeTeamScore": 0,
                    "LastPlay": "",
                    "Location": "",
                    "NumberOfGamesInSeries": 1,
                    "SeriesInfo": null,
                    "SeriesType": 0,
                    "Sport": "GOLF",
                    "StartDate": "2018-04-05T10:00:00.0000000Z",
                    "Status": "Pregame",
                    "TeamWithPossession": 0,
                    "TimeRemainingStatus": ""
                }
            ],
            "ContestStartTimeSuffix": " (PGA)",
            "GameSetKey": "C21E484C4CAC884B1FDA39650C6BF0E8",
            "GameStyles": [
                {
                    "Abbreviation": "CLA",
                    "Description": "Create a 6-golfer lineup while staying under the $50,000 salary cap",
                    "GameStyleId": 20,
                    "IsEnabled": true,
                    "Name": "Classic",
                    "SortOrder": 1,
                    "SportId": 13
                }
            ],
            "MinStartTime": "/Date(1522922400000)/",
            "SortOrder": null,
            "Tag": "Featured"
        }
    ],
    "GameTypes": [
        {
            "Description": "Create an 8-player lineup while staying under the $50,000 salary cap",
            "DraftType": "SalaryCap",
            "GameStyle": {
                "Abbreviation": "CLA",
                "Description": "Create an 8-player lineup while staying under the $50,000 salary cap",
                "GameStyleId": 5,
                "IsEnabled": true,
                "Name": "Classic",
                "SortOrder": 1,
                "SportId": 4
            },
            "GameTypeId": 3,
            "Name": "Classic",
            "SportId": 4,
            "Tag": ""
        },
        {
            "Description": "Create a 6-driver lineup while staying under the $50,000 salary cap",
            "DraftType": "SalaryCap",
            "GameStyle": {
                "Abbreviation": "CLA",
                "Description": "Create a 6-driver lineup while staying under the $50,000 salary cap",
                "GameStyleId": 25,
                "IsEnabled": true,
                "Name": "Classic",
                "SortOrder": 1,
                "SportId": 10
            },
            "GameTypeId": 8,
            "Name": "Classic",
            "SportId": 10,
            "Tag": ""
        }
    ],
    "IsVip": null,
    "MarketingOffers": [
    ],
    "PrizeRedemptionModel": null,
    "PrizeRedemptionPop": false,
    "SelectedSport": null,
    "ShowAds": true,
    "ShowRafLink": false,
    "SportMenuItems": null,
    "UseGameSetFilter": false,
    "UseRaptorHeadToHead": false,
    "UserGeoLocation": "super cool place."
}
"""
