# pylint: disable=no-self-use
"""
Handler for the draft kings service. Provides a consistent API to the draft kings service that it
fufills by making http resquests to draftkings.com
"""

from datetime import datetime

import json
import pytz
import requests

from objects import ttypes as objects
from utils import enum_convert
from utils import draft_kings as dk_util

DRAFT_KINGS_URL = 'https://www.draftkings.com'
CONTEST_URL_API = '%s/lobby/getcontests' % DRAFT_KINGS_URL
DRAFTABLES_URL = 'https://api.draftkings.com/draftgroups/v1/draftgroups/%s/draftables?format=json'


class InvalidGameType(Exception):
  """Raised when we can't determine the correct game type for returning number of positions."""
  pass


def _get_num_positions(league, score_type):
  """Gets the number of positions based on the league and score type.

  This is hard coded for now, it doesn't appear readily available from the Contests json returned by
  draft kings and doesn't seem to change often/ever.

  Args:
    league (objects.League): League to get number of positions for.
    score_type (objects.ScoreType): Score type to get number of positions for.

  Returns:
    (int) number of positions in the contest.

  """
  lea = objects.League
  s_t = objects.ScoreType
  mapping = {
      (lea.NBA, s_t.CLASSIC): 8,
      (lea.NHL, s_t.CLASSIC): 8,
      (lea.LOL, s_t.CLASSIC): 8,
      (lea.NBA, s_t.SHOWDOWN): 8,
      (lea.NHL, s_t.SHOWDOWN): 8,
      (lea.LOL, s_t.SHOWDOWN): 8,
      (lea.NBA, s_t.PICK_EM): 8,
      (lea.NHL, s_t.PICK_EM): 8,
      (lea.LOL, s_t.PICK_EM): 8,
      (lea.NBA, s_t.LATE_SWAP): 8,
      (lea.NHL, s_t.LATE_SWAP): 8,
      (lea.LOL, s_t.LATE_SWAP): 8,
  }
  if (league, score_type) in mapping.keys():
    return mapping[(league, score_type)]

  raise InvalidGameType('Could not determine number of positions for League: %s, ScoreType %s' %
                        (league, score_type))


def _get_contest_json_for_league(league):
  """Helper function for getting the draft kings contest json for a league"""
  response = requests.get(url=CONTEST_URL_API, params={'sport':
                                                       enum_convert.league_to_str(league)})
  response.raise_for_status()
  return response.content


def _get_player_json_for_contest(contest):
  """Gets the player json for a contest by sending a request to the draft kings api.

  Args:
    contest (objects.ContestSpec): Contest specification to get players for.

  Returns:
    (str) A json string containing the data for the avaialable players for a contest.
  """
  response = requests.get(url=DRAFTABLES_URL % contest.dk_spec.draft_group_id)
  response.raise_for_status()
  return response.content


def _convert_dk_data_string(date_string):
  time_part = date_string.split('(')[1].split(')')[0]
  return str(datetime.fromtimestamp(timestamp=long(time_part) / 1000, tz=pytz.utc))


def _contest_spec_from_dk_contest(contest, league):
  request_league = enum_convert.league_from_dk_id(contest['s'])
  if request_league != league:
    return None
  dk_spec = objects.DraftKingSpec(contest_id=contest['id'], draft_group_id=contest['dg'])
  score_type = enum_convert.str_to_score_type(contest['gameType'])
  return objects.ContestSpec(
      salary_cap=50000,
      num_positions=_get_num_positions(league, score_type),
      name=contest['n'].encode('utf-8'),
      start_time=_convert_dk_data_string(contest['sd']),
      entry_fee=contest['a'],
      total_payout=contest['po'],
      total_entries=contest['nt'],
      max_entries=contest['m'],
      site=objects.ContestSite.DRAFT_KINGS,
      dk_spec=dk_spec,
      league=league,
      score_type=score_type
  )


def _athlete_from_dk_draftable(draftable, league):
  """Gets an active athlete from a draftable json object from the draft kings website."""
  positions = [dk_util.position_from_str(pos, league) for pos in draftable['position'].split('/')]
  if 0 in positions or 1 in positions:
    positions.append(5)
  if 2 in positions or 3 in positions:
    positions.append(6)
  positions.append(7)
  name = '%s %s' % (draftable['firstName'], draftable['lastName'])
  return objects.ActiveAthlete(
      objects.Athlete(name.encode('utf-8'), league, positions),
      int(draftable['salary']),
      0.0,
  )


class HttpHandler(object):
  """Defines functions for handling the draft_kings service thrift api."""

  def get_contests(self, league):
    """Gets all contests for draft kings for the given league that could potentially be entered.

    Args:
      league (objects.League): Leage to collect available contests for.

    Returns:
      ([objects.ContestSpec]) top num_results lineups ranked by score.
    """
    con_map = json.loads(_get_contest_json_for_league(league))
    contests = [_contest_spec_from_dk_contest(contest, league) for contest in con_map['Contests']]
    return [c for c in contests if c]

  def get_athletes_for_contest(self, contest):
    """Gets the athlete participating in a specfic contests.

    Args:
      contest (objects.ContestSpec): The contest to get athletes for.

    Returns:
      ([objects.ActiveAthlete])
    """
    result = json.loads(_get_player_json_for_contest(contest))
    athletes = [_athlete_from_dk_draftable(draftable, contest.league) for draftable in
                result['draftables']]
    return [a for a in athletes if a]

  def ping(self):
    """Handles the ping call, simply returns true.

    Returns:
      (bool) Always returns true.
    """
    return True
