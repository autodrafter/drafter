"""
Entry point for the collection service.
"""


from draft_kings_service import DraftKings
from draft_kings import http_handler
from utils import thrift_util, config_util


if __name__ == '__main__':
  thrift_util.start_server(http_handler.HttpHandler(),
                           config_util.get_service_configs('test')['draft_kings'],
                           DraftKings)
