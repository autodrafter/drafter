set nocompatible 
set nobackup
set noswapfile
syntax on
set nowrap
set encoding=utf-8

set colorcolumn=100

set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

"Core plugins
Plugin 'kien/ctrlp.vim'
Plugin 'flazz/vim-colorschemes'

"Helpfull
Plugin 'scrooloose/nerdtree'
Plugin 'easymotion/vim-easymotion'
Plugin 'tpope/vim-fugitive'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'bling/vim-airline'
Plugin 'Valloric/YouCompleteMe'
Plugin 'airblade/vim-gitgutter'
Plugin 'rdnetto/YCM-Generator'

Plugin 'tpope/vim-dispatch'
Plugin 'scrooloose/syntastic'

"Luangagues
Plugin 'davidhalter/jedi-vim'
Plugin 'tell-k/vim-autopep8'
Plugin 'solarnz/thrift.vim'

"Snipets
Plugin 'SirVer/ultisnips'


call vundle#end() 
set autoindent

"Run any init.vim script in directory
if findfile("init.vim", ".") == "init.vim"
    autocmd VimEnter * source init.vim
endif

"ignores
let g:ctrlp_custom_ignore = '\v[\/](node_modules|target|dist|build|bin|genpy)|(\.(swp|ico|git|svn|pyc))$'

"Nnoremap
nnoremap <F5> :NERDTreeToggle<CR>
nnoremap <c-f> :CtrlP<CR>
nnoremap <c-t> :<CR>

"Maps
map <space> <Plug>(easymotion-prefix)
command WQ wq
command Wq wq
command W w
command Q q

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<c-f>"
let g:UltiSnipsJumpForwardTrigger = "<c-f>"
let g:UltiSnipsJumpBackwardTrigger = "<c-d>"
let g:ultisnips_python_style = 'google'

" autopep stuff
let g:autopep8_indent_size=2
let g:autopep8_disable_show_diff=1 
autocmd FileType python noremap <leader>f  :call Autopep8()<CR>
let g:autopep8_max_line_length=99
