"""
Common functionality used accross the different service clients.
"""
from thrift.transport import TSocket, TTransport
from thrift.protocol import TBinaryProtocol

from objects.ttypes import ThriftProtocol, ThriftTransport


def add_ping_sub_command(service_name, sub_parsers):
  """Adds a common ping sub command for service_name.

  Args:
    service_name (str): Service name to add pint subcommand for.
    sub_parsers (argparse.subparsers): Subparsers object from the argparse library to add ping
        subcommand to.

  Returns: (argparse.argument) Created argparse argument.
  """

  ping = sub_parsers.add_parser('%s_ping' % service_name,
                                help='Pings the %s service.' % service_name)
  return ping


def init_thrift_client(config):
  """Returns a thrift protocol and transport for use with a client based on a thrift config proto.

  Args:
    config (objects.ThriftServiceConfig): Service definition to set up thrift protocol with.

  Returns: ((thrift.protocol, thrift.transport))

  """
  socket = TSocket.TSocket(config.host_name, config.host_port)
  if config.transport is ThriftTransport.BUFFERED:
    transport = TTransport.TBufferedTransport(socket)
  if config.protocol is ThriftProtocol.BINARY:
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
  return (protocol, transport)
