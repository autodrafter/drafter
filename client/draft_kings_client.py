"""
Handles the logic for the draft kings service cli.
"""

from client import common
from draft_kings_service import DraftKings
from utils import enum_convert


def add_sub_commands(sub_parsers):
  """Adds the subcommands for the draft kings service.

  Args:
    sub_parsers (argparse.sub_parsers): Subparsers object from the argparse library to add
        collection service subcommands to.

  Returns:
    None
  """
  get_contests = sub_parsers.add_parser(
      'dk_get_contest_specs',
      help="Gets ContestSpecs for the currently active draft kings contests.")
  get_contests.add_argument('--league', dest='league', default='', type=str, required=True,
                            help='League to get contets for, current valid values are NBA|NHL|LOL')
  get_contests.set_defaults(func=handle_get_contests)

  common.add_ping_sub_command('draft_kings', sub_parsers).set_defaults(func=handle_ping)


def handle_get_contests(args, configs):
  """Handles a get_contests call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  protocol, transport = common.init_thrift_client(configs['draft_kings'])
  client = DraftKings.Client(protocol)
  transport.open()
  contests = client.get_contests(enum_convert.str_to_league(args.league))
  for contest in contests:
    print contest


def handle_ping(args, configs):
  """Handles a draft_kings_ping cli call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  del args
  protocol, transport = common.init_thrift_client(configs['draft_kings'])
  client = DraftKings.Client(protocol)
  transport.open()
  ping = client.ping()
  transport.close()
  print 'Ping result: %s' % ping
