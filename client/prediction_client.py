"""
Handles the logic for the prediction service cli.
"""

from client import common
from prediction_service import Predictor
from objects import ttypes as objects
from utils import enum_convert


def add_sub_commands(sub_parsers):
  """Adds the subcommands for the prediction service.

  Args:
    sub_parsers (argparse.sub_parsers): Subparsers object from the argparse library to add
        collection service subcommands to.

  Returns:
    None
  """
  nba_predict = sub_parsers.add_parser(
      'predict_performances', help='Predicts the next performances for a list of athletes.')
  nba_predict.add_argument('--athletes', dest='athletes', default='', type=str, nargs='+',
                           help='List of athlete names to predict performaces for.', required=True)
  nba_predict.add_argument('--score_type', dest='score_type', default='', type=str,
                           help='Scoring type to predict their fantasy scores with.', required=True)
  nba_predict.set_defaults(func=handle_predict)

  common.add_ping_sub_command('predictor', sub_parsers).set_defaults(func=handle_ping)


def handle_predict(args, configs):
  """Handles a get_nba_performace cli call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  protocol, transport = common.init_thrift_client(configs['prediction'])
  client = Predictor.Client(protocol)
  transport.open()
  athletes = [objects.Athlete(full_name=athlete, league=objects.League.NBA)
              for athlete in args.athletes]
  active_athletes = client.predict_performances(
      athletes,
      enum_convert.str_to_score_type(args.score_type))
  transport.close()
  for athlete in active_athletes:
    print athlete


def handle_ping(args, configs):
  """Handles a predictor_ping cli call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  del args
  protocol, transport = common.init_thrift_client(configs['prediction'])
  client = Predictor.Client(protocol)
  transport.open()
  ping = client.ping()
  transport.close()
  print 'Ping result: %s' % ping
