"""
Handles the logic for the collection service cli. It adds subcommands to the argument parser and
has control passed to it when the 'collection *' subcommands are invoked.
"""

from collection_service import Collector
from client import common


def add_sub_commands(sub_parsers):
  """Adds the subcommands for the collection service.

  Args:
    sub_parsers (argparse.sub_parsers): Subparsers object from the argparse library to add
        collection service subcommands to.

  Returns:
    None
  """
  nba_perf = sub_parsers.add_parser(
      'get_nba_performances', help='Gets the performance information for a list of nba athletes.')
  nba_perf.add_argument('--athletes', dest='athletes', default='', type=str, nargs='+',
                        help='List of athlete names to look up performaces for.', required=True)
  nba_perf.add_argument('--num_perfs', dest='num_perfs', default=3, type=int,
                        help='Number of performances to look up for each athlete.')
  nba_perf.set_defaults(func=handle_nba_perf)

  common.add_ping_sub_command('collector', sub_parsers).set_defaults(func=handle_ping)


def handle_ping(args, configs):
  """Handles a collector_ping cli call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  del args
  protocol, transport = common.init_thrift_client(configs['collection'])
  client = Collector.Client(protocol)
  transport.open()
  ping = client.ping()
  transport.close()
  print 'Ping result: %s' % ping


def handle_nba_perf(args, configs):
  """Handles a get_nba_performace cli call.

  Args:
    args (argparse.args): The parsed arguments from a cli invocation.
    configs {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service
      configs.

  Returns:
    None
  """
  protocol, transport = common.init_thrift_client(configs['collection'])
  client = Collector.Client(protocol)
  transport.open()
  performances = client.get_nba_performances(args.athletes, args.num_perfs)
  transport.close()
  for performance in performances:
    print performance
