#pylint: disable=missing-docstring
"""
Tests for client.collection_client module.
"""

import argparse
import unittest
from StringIO import StringIO

import mock
from parameterized import parameterized

from client import collection_client
from utils import testing


class TestCollectionClient(unittest.TestCase):
  """client.collection_client tests."""

  def setUp(self):
    self.arg_parser = argparse.ArgumentParser(description='Test Parser.')
    self.sub_parsers = self.arg_parser.add_subparsers()

  def test_add_sub_commands_basic(self):
    collection_client.add_sub_commands(self.sub_parsers)
    self.assertIn('get_nba_performances', self.sub_parsers.choices)
    self.assertIn('collector_ping', self.sub_parsers.choices)

  @parameterized.expand([(True,), (False,)])
  @mock.patch('client.common.init_thrift_client')
  @mock.patch('collection_service.Collector.Client')
  @mock.patch('sys.stdout', new_callable=StringIO)
  def test_handle_ping(self, ping_value, mock_stdout, mock_client, mock_init):
    mock_ping_client = mock.Mock()
    mock_ping_client.ping.return_value = ping_value
    mock_client.return_value = mock_ping_client
    mock_init.return_value = (mock.Mock(), mock.Mock())

    collection_client.handle_ping({}, {'collection': 2})

    mock_ping_client.ping.assert_called_once()
    self.assertEqual('Ping result: %s\n' % ping_value, mock_stdout.getvalue())

  @parameterized.expand([
      (['ted', 'bill', 'jim'],),
      (['bob'],),
  ])
  @mock.patch('client.common.init_thrift_client')
  @mock.patch('collection_service.Collector.Client')
  @mock.patch('sys.stdout', new_callable=StringIO)
  def test_handle_nba_get(self, player_names, mock_stdout, mock_client, mock_init):
    mock_predict_client = mock.Mock()
    mock_args = mock.Mock()
    mock_args.athletes = player_names
    mock_args.num_perfs = 3
    performances = [testing.performance_from_name(name) for name in player_names]
    mock_predict_client.get_nba_performances.return_value = performances
    mock_client.return_value = mock_predict_client
    mock_init.return_value = (mock.Mock(), mock.Mock())

    collection_client.handle_nba_perf(mock_args, {'collection': 3})


    mock_predict_client.get_nba_performances.assert_called_once_with(player_names, 3)
    self.assertEqual('\n'.join(str(p) for p in performances) + '\n', mock_stdout.getvalue())
