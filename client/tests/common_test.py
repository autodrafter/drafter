#pylint: disable=no-member, protected-access, missing-docstring
"""
Tests for client.common module.
"""
import argparse
import unittest

from parameterized import parameterized

from client import common
from objects import ttypes as objects


class TestCommon(unittest.TestCase):
  """client.common unit tests. """

  def setUp(self):
    self.arg_parser = argparse.ArgumentParser(description='Test Parser.')
    self.sub_parsers = self.arg_parser.add_subparsers()

  def test_add_ping_basic(self):
    result = common.add_ping_sub_command('test', self.sub_parsers)
    self.assertIn('test_ping', result.prog)
    self.assertEqual(result, self.sub_parsers.choices['test_ping'])

  @parameterized.expand([
      ('cool', 10),
      ('test.com', 9090),
  ])
  def test_init_thrift_client(self, host, port):
    config = objects.ThriftServiceConfig(
        host_name=host,
        host_port=port,
        transport=objects.ThriftTransport.BUFFERED,
        protocol=objects.ThriftProtocol.BINARY)
    protocol, transport = common.init_thrift_client(config)
    self.assertEqual(host, transport._TBufferedTransport__trans.host)
    self.assertEqual(port, transport._TBufferedTransport__trans.port)
    self.assertEqual(transport, protocol.trans)
