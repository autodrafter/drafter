# pylint: disable=missing-docstring, too-many-arguments
"""
Tests for client.prediction_client module.
"""

import argparse
import unittest
from StringIO import StringIO

import mock
from parameterized import parameterized

from client import prediction_client
from objects import ttypes as objects
from utils import enum_convert


class TestPredictionClient(unittest.TestCase):
  """client.prediction_client tests."""

  def setUp(self):
    self.arg_parser = argparse.ArgumentParser(description='Test Parser.')
    self.sub_parsers = self.arg_parser.add_subparsers()

  def test_add_sub_commands_basic(self):
    prediction_client.add_sub_commands(self.sub_parsers)
    self.assertIn('predict_performances', self.sub_parsers.choices)
    self.assertIn('predictor_ping', self.sub_parsers.choices)

  @parameterized.expand([(True,), (False,)])
  @mock.patch('client.common.init_thrift_client')
  @mock.patch('prediction_service.Predictor.Client')
  @mock.patch('sys.stdout', new_callable=StringIO)
  def test_handle_ping(self, ping_value, mock_stdout, mock_client, mock_init):
    mock_ping_client = mock.Mock()
    mock_ping_client.ping.return_value = ping_value
    mock_client.return_value = mock_ping_client
    mock_init.return_value = (mock.Mock(), mock.Mock())

    prediction_client.handle_ping({}, {'prediction': 2})

    mock_ping_client.ping.assert_called_once()
    self.assertEqual('Ping result: %s\n' % ping_value, mock_stdout.getvalue())

  @parameterized.expand([
      (['ted', 'bill', 'jim'], 'CLASSIC'),
      (['bob'], 'CLASSIC'),
  ])
  @mock.patch('client.common.init_thrift_client')
  @mock.patch('prediction_service.Predictor.Client')
  @mock.patch('sys.stdout', new_callable=StringIO)
  def test_handle_nba_predict(self, athlete_names, score_type, mock_stdout, mock_client, mock_init):
    athletes = [objects.Athlete(full_name=athlete, league=objects.League.NBA)
                for athlete in athlete_names]
    mock_predict_client = mock.Mock()
    mock_args = mock.Mock()
    mock_args.athletes = athlete_names
    mock_args.score_type = score_type
    mock_client.return_value = mock_predict_client
    mock_init.return_value = (mock.Mock(), mock.Mock())
    expected = [objects.ActiveAthlete(ath, 100, 12.0) for ath in athletes]

    mock_predict_client.predict_performances.return_value = expected

    prediction_client.handle_predict(mock_args, {'prediction': 3})

    mock_predict_client.predict_performances.assert_called_once_with(
        athletes,
        enum_convert.str_to_score_type(score_type))
    self.assertEqual('\n'.join(str(e) for e in expected) + '\n', mock_stdout.getvalue())
