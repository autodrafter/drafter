"""
Entry point for the collection service.
"""


from lineup_service import LineupGenerator
from lineup import brute_force_handler
from utils import thrift_util, config_util


if __name__ == '__main__':
  thrift_util.start_server(brute_force_handler.BruteForceHandler(),
                           config_util.get_service_configs('test')['lineup'],
                           LineupGenerator)
