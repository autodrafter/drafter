# pylint: disable=missing-docstring
"""
Tests for brute force lineup handler.
"""

import unittest
from parameterized import parameterized

from lineup import brute_force_handler
from objects import ttypes as objects
from utils import testing


class TestBruteForceLineupHandler(unittest.TestCase):
  """Tests for BruteForceHandler."""

  def setUp(self):
    self.test_athletes = []
    self.test_athletes.append(testing.get_active_athlete('Bob', [0, 2], 14.0, 2.2))
    self.test_athletes.append(testing.get_active_athlete('Jim', [0, 1, 2], 12.0, 12.2))
    self.test_athletes.append(testing.get_active_athlete('Kyle', [0], 10.0, 6.2))
    self.test_athletes.append(testing.get_active_athlete('Doodie', [0, 1], 5.5, 22.2))
    self.test_athletes.append(testing.get_active_athlete('MacBush', [1, 2], 20.0, 100.2))
    self.test_athletes.append(testing.get_active_athlete('Hubert', [0, 1, 2], 10.0, 2.2))

  def test_simple_construction(self):
    self.assertIsNotNone(brute_force_handler.BruteForceHandler())

  def test_ping(self):
    self.assertTrue(brute_force_handler.BruteForceHandler().ping())

  @parameterized.expand([
      (10, 300.0),
      (2, 200.0),
      (20, 100.0),
  ])
  def test_get_top_lineups_basic(self, num_results, salary_cap):
    c_spec = objects.ContestSpec()
    c_spec.num_positions = 3
    c_spec.salary_cap = salary_cap

    result = brute_force_handler.BruteForceHandler().get_top_lineups(self.test_athletes,
                                                                     c_spec, num_results)

    salaries = [testing.get_lineup_cost(lineup) for lineup in result]
    for salary in salaries:
      self.assertLessEqual(salary, salary_cap)
    self.assertEqual(len(result), num_results)
    for i in range(1, num_results):
      self.assertGreaterEqual(result[i - 1].score, result[i].score)

  @parameterized.expand([
      (0, 300.0, 3),
      (100, 2.0, 3),
      (20, 100.0, 4),
  ])
  def test_get_top_lineups_no_results(self, num_results, salary_cap, num_positions):
    c_spec = objects.ContestSpec()
    c_spec.num_positions = num_positions
    c_spec.salary_cap = salary_cap

    result = brute_force_handler.BruteForceHandler().get_top_lineups(self.test_athletes,
                                                                     c_spec, num_results)

    self.assertFalse(result)

  def test_get_top_lineups_top_score(self):
    c_spec = objects.ContestSpec()
    c_spec.num_positions = 3
    c_spec.salary_cap = 100000.0

    result = brute_force_handler.BruteForceHandler().get_top_lineups(self.test_athletes,
                                                                     c_spec, 1)

    self.assertEqual(46, result[0].score)
