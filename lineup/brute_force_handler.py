# pylint: disable=no-self-use
"""
Handler for the lineup service. Brute forces the optimal lineups by trying every possible lineup for
the given players.
"""

import itertools

from objects import ttypes as objects


class BruteForceHandler(object):
  """Defines the functions needed to handle the lineup_service api, brute forces optimal lineups."""

  def get_top_lineups(self, athletes, contest, num_results):
    """Brute forces the top num_results lineups based on the provided athletes and contest_spec.

    Args:
      athletes (objects.ActiveAthlete): Info on each athlete to optimize for, does not have
        predicted_points populated.
      contest (objects.ContestSpec): Specification for the type of contest to optimize for
      num_results (int): Number of top results to return.

    Returns:
      ([objects.Lineup]) top num_results lineups ranked by score.
    """
    lineups = []
    athletes_by_pos = []

    # Group the athletes by positions they can play.
    for i in range(0, contest.num_positions):
      athletes_at_position = []
      for athlete in athletes:
        if i in athlete.athlete.positions:
          athletes_at_position.append(athlete)
      athletes_by_pos.append(athletes_at_position)

    for lineup in itertools.product(*athletes_by_pos):
      # If a player exists more than once, not a valid lineup, continue.
      if len(set([a.athlete.full_name for a in lineup])) < len(lineup):
        continue
      # If the lineup is over the salary cap, not a valid lineup, continue.
      if sum(a.cost for a in lineup) > contest.salary_cap:
        continue

      score = sum(a.predicted_points for a in lineup)
      # Keep a sorted list of top lineup scores, insert the new score in the approriate position,
      # then pop off the worst score.
      i = 0
      for line in lineups:
        if score > line.score:
          break
        i += 1
      lineups.insert(i, objects.Lineup(athletes=lineup, score=score))
      lineups = lineups[:num_results]

    return lineups

  def ping(self):
    """Handles the ping call, simply returns true.

    Returns:
      (bool) Always returns true.
    """
    return True
