"""
Handler for the collection service that returns dummy information mostly used for testing and
initial setup.
"""

from objects.ttypes import NbaPerformance, Athlete, NbaGameStats, League


class SimpleCollectionHandler(object):
  """Defines the functions needed to handle the collection_service api, but returns dummy info."""

  def get_nba_performances(self, athlete_names, num_performances):
    """Returns fake performances for each athlete for each performance.

    Args:
      athlete_names ([str]): List of athlete names to return performances for.
      num_performances (int): Number of performances to return.

    Returns:
      [objects.nba_performances] A list of objects.nba_performance, dummy info in this case.
    """
    performances = []
    for athlete_name in athlete_names:
      for j in range(num_performances):
        athlete = Athlete(full_name=athlete_name, league=League.NBA)
        stats = NbaGameStats(points=j, assists=j, steals=j, blocks=j, three_pointers=j, rebounds=j,
                             turnovers=j)
        performances.append(NbaPerformance(athlete=athlete, stats=stats))
    return performances

  def ping(self):
    """Handles the ping call, simply returns true.

    Returns:
      (bool) Always returns true.
    """
    return True
