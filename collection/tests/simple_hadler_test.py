#pylint: disable=missing-docstring
"""
Tests for simple_handler.
"""

import unittest
from parameterized import parameterized

from collection.simple_handler import SimpleCollectionHandler
from utils import testing


class TestSimpleCollectionHandler(unittest.TestCase):
  """Tests for SimpleCollectionHandler."""

  def test_simple_construction(self):
    self.assertIsNotNone(SimpleCollectionHandler())

  @parameterized.expand([
      ([], 2),
      (['jeff'], 1),
      (['jim', 'bob', 'jeff'], 3),
  ])
  def test_get_nba_performance(self, athlete_names, num_perfs):
    expected = []
    for j in range(num_perfs):
      expected += [testing.performance_from_name(athlete, j) for athlete in athlete_names]

    result = SimpleCollectionHandler().get_nba_performances(athlete_names, num_perfs)

    self.assertItemsEqual(expected, result)

  def test_ping(self):
    self.assertTrue(SimpleCollectionHandler().ping())
