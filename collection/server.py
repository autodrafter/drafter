"""
Entry point for the collection service.
"""

from collection_service import Collector
from collection import simple_handler
from utils import thrift_util, config_util


if __name__ == '__main__':
  thrift_util.start_server(simple_handler.SimpleCollectionHandler(),
                           config_util.get_service_configs('test')['collection'],
                           Collector)
