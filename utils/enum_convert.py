"""
Utils for converting between various thrift enums.
"""

from objects import ttypes as objects


class InvalidEnumType(Exception):
  """Raised when an invalid enum type is attempted to be converted."""
  pass


def str_to_score_type(value):
  """Converts a string into the proper thrift ScoreType enum.

  Args:
    value (str): String enum representation

  Returns:
    (objects.ScoreType) converted thrift score type enum.
  """
  if value and value.lower() == 'classic':
    return objects.ScoreType.CLASSIC
  if value and value.lower() == 'showdown':
    return objects.ScoreType.SHOWDOWN
  if value and value.lower() == 'pick\'em':
    return objects.ScoreType.PICK_EM
  if value and value.lower() == 'late swap':
    return objects.ScoreType.LATE_SWAP
  raise InvalidEnumType('Could not convert "%s" to an enum' % value)


def league_to_str(league):
  """Converts a league type to a draft kings api compatible string.

  Args:
    league (objects.League): League to convert

  Returns:
    (str) valid draft kings string representation.

  """
  league_string_map = {
      objects.League.NBA: 'nba',
      objects.League.NFL: 'nfl',
      objects.League.NHL: 'nhl',
      objects.League.NASCAR: 'nas',
      objects.League.SOCCER: 'soc',
      objects.League.GOLF: 'golf',
      objects.League.CFL: 'cfl',
      objects.League.LOL: 'lol',
  }
  return league_string_map[league]


def str_to_league(league_str):
  """Converts a string into a league type.

  Args:
    leage_str (str): String representation of a league.

  Returns:
    (objects.League) League from string
  """
  string_league_map = {
      'nba':  objects.League.NBA,
      'nfl': objects.League.NFL,
      'nhl': objects.League.NHL,
      'nas': objects.League.NASCAR,
      'soc': objects.League.SOCCER,
      'golf': objects.League.GOLF,
      'cfl': objects.League.CFL,
      'lol': objects.League.LOL,
  }
  return string_league_map[league_str.lower()]


def league_from_dk_id(dk_id):
  """Gets the correct league based on a draft king id.

  Args:
    dk_id (int): Draft king id to translate to a league.

  Returns:
    (objects.League) Leage for corresponding draft king id.
  """
  dk_id_map = {
      1: objects.League.NFL,
      3: objects.League.NHL,
      4: objects.League.NBA,
      10: objects.League.NASCAR,
      11: objects.League.LOL,
      12: objects.League.SOCCER,
      13: objects.League.GOLF,
      14: objects.League.CFL,
  }
  if dk_id not in dk_id_map:
    return objects.League.UNKNOWN
  return dk_id_map[dk_id]
