"""
Util functions for working with configurations.
"""

import os

from objects import ttypes as objects
from utils import thrift_util


class NoValidConfigs(Exception):
  """Raised when an empty dictionary is left after attemptingn to lad the service configs."""
  pass


def get_service_configs(universe):
  """Reads and loads configurations for the various drafter services for the supplied universe.

  Args:
    universe (str): Universe to load configs for (prod|test)

  Returns:
    {str->objects.ThriftServiceConfig} Dictionary mapped by service name containing service configs.
  """
  configs = {}
  path = 'configs/service_%s' % universe
  if os.path.exists(path):
    for file_name in os.listdir(path):
      service = file_name.split('.')[0]
      file_path = os.path.join(path, file_name)
      configs[service] = thrift_util.read_from_file_json(file_path, objects.ThriftServiceConfig)

  if not configs:
    raise NoValidConfigs('No valid configs found for universe: %s path: %s' % (universe, path))

  return configs
