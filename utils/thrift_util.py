"""
Utils for working with thrift.
"""
import json

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer
from thrift_json import ThriftJSONEncoder, ThriftJSONDecoder

from objects import ttypes as objects


def start_server(handler, config, service):
  """Starts a thrift collection_service with the supplied config and handler.

  Args:
    handler (object): Class that implements the functions defined by collection_service.Collector
    config (objects.ThriftServiceConfig): Thrift service configuration defining how to start the
        servserver.
    service (thrift.Service): Thirft service to start

  Returns: None

  """
  processor = service.Processor(handler)
  socket = TSocket.TServerSocket(port=config.host_port)
  if config.transport == objects.ThriftTransport.BUFFERED:
    tfactory = TTransport.TBufferedTransportFactory()
  if config.protocol == objects.ThriftProtocol.BINARY:
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
  server = TServer.TSimpleServer(processor, socket, tfactory, pfactory)
  server.serve()


def serialize_to_bytes(thrift_object):
  """Serialized a thrift object to its byte representation.

  Args:
    thrift_object (thrift.Object): Obect to serialize

  Returns:
    [byte] byte array for the object

  """
  transport_out = TTransport.TMemoryBuffer()
  protocol_out = TBinaryProtocol.TBinaryProtocol(transport_out)
  thrift_object.write(protocol_out)
  return transport_out.getvalue()


def write_to_file_json(thrift_object, file_path):
  """Writes thrift object out to a file after encoding it as json.

  Args:
    thrift_object (thrift.Object): Object to encode as json and write.
    file_path (str): Path to write object to.

  Returns:
    None
  """
  with open(file_path, 'w') as out_file:
    out_file.write(json.dumps(thrift_object, cls=ThriftJSONEncoder))


def read_from_file_json(file_path, thrift_class):
  """Reads a json encoded thrift object from a file.

  Args:
    file_path (str): Path to file to read from.
    thrift_class (cls): Thrift object type that is being read.

  Returns:
    (thrift_class) A thrift object of type thrift_class that has been read from file_path.
  """
  with open(file_path, 'r') as read_file:
    return json.loads(read_file.read(), cls=ThriftJSONDecoder, thrift_class=thrift_class)
