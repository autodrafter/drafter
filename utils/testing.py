"""
Common testing utils.
"""

from objects import ttypes as objects


def performance_from_name(name, value=0.0):
  """Gets a defaulted performance for a given name."""
  athlete = objects.Athlete(full_name=name, league=objects.League.NBA)
  stats = objects.NbaGameStats(points=value, assists=value, steals=value, blocks=value,
                               three_pointers=value, rebounds=value, turnovers=value)
  return objects.NbaPerformance(athlete=athlete, stats=stats)


def get_active_athlete(name, positions, points, cost):
  """Helper function to generate a fake active athlete more easily."""
  athlete = objects.ActiveAthlete(cost=cost, predicted_points=points, athlete=objects.Athlete())
  athlete.athlete.full_name = name
  athlete.athlete.positions = positions
  return athlete

def get_lineup_cost(lineup):
  """Gets the total cost of a lineup."""
  return sum(athlete.cost for athlete in lineup.athletes)
