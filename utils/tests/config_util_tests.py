# pylint: disable=missing-docstring
import unittest

from utils import config_util


class ConfigUtilTest(unittest.TestCase):

  def test_reads_valid_universe(self):
    result = config_util.get_service_configs('test')
    self.assertEqual(len(result), 4)

  def test_invalid_universe_exception(self):
    with self.assertRaises(config_util.NoValidConfigs):
      config_util.get_service_configs('bad_name')
