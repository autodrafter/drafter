# pylint: disable=missing-docstring
import unittest
from parameterized import parameterized

from utils import testing


class TestTestingUtil(unittest.TestCase):

  @parameterized.expand([
      ('jeff', 0),
      ('jeff', 3),
      ('bob', 1.2),
      ('', 110),
  ])
  def test_performance_from_name(self, name, value):
    result = testing.performance_from_name(name, value)
    self.assertEqual(result.athlete.full_name, name)
    self.assertEqual(result.stats.points, value)
    self.assertEqual(result.stats.assists, value)
    self.assertEqual(result.stats.steals, value)
    self.assertEqual(result.stats.blocks, value)
    self.assertEqual(result.stats.three_pointers, value)
    self.assertEqual(result.stats.rebounds, value)
    self.assertEqual(result.stats.turnovers, value)
