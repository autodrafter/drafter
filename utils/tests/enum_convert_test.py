# pylint: disable=missing-docstring
import unittest
from parameterized import parameterized

from objects import ttypes as objects
from utils import enum_convert


class EnumConvertUtilTests(unittest.TestCase):

  @parameterized.expand([
      ('CLASSIC', objects.ScoreType.CLASSIC),
      ('Pick\'Em', objects.ScoreType.PICK_EM),
      ('classic', objects.ScoreType.CLASSIC),
      ('Showdown', objects.ScoreType.SHOWDOWN),
  ])
  def test_str_to_score_type_valid(self, value, expected):
    result = enum_convert.str_to_score_type(value)
    self.assertEqual(expected, result)

  @parameterized.expand([
      (None,),
      ('NBA_CLASSC',),
  ])
  def test_str_to_score_type_invalid(self, value):
    with self.assertRaises(enum_convert.InvalidEnumType):
      enum_convert.str_to_score_type(value)

  @parameterized.expand([
      (1, objects.League.NFL),
      (3, objects.League.NHL),
      (4, objects.League.NBA),
      (10, objects.League.NASCAR),
      (11, objects.League.LOL),
      (12, objects.League.SOCCER),
      (13, objects.League.GOLF),
      (14, objects.League.CFL),
      (111, objects.League.UNKNOWN),
      (-4, objects.League.UNKNOWN),
      (None, objects.League.UNKNOWN),
  ])
  def league_from_dk_id(self, value, expected):
    self.assertEqual(expected, enum_convert.str_to_score_type(value))

  @parameterized.expand([
      ('NBA', objects.League.NBA),
      ('nfl', objects.League.NFL),
      ('nhl', objects.League.NHL),
      ('nas', objects.League.NASCAR),
      ('soc', objects.League.SOCCER),
      ('golf', objects.League.GOLF),
      ('cfl', objects.League.CFL),
      ('lol', objects.League.LOL),
  ])
  def test_str_to_league(self, value, expected):
    self.assertEqual(expected, enum_convert.str_to_league(value))

  @parameterized.expand([
      (None,),
      ('NBA_CLASSC',),
  ])
  def test_str_to_leage_error(self, value):
    with self.assertRaises(Exception):
      enum_convert.str_to_league(value)

  @parameterized.expand([
      ('nba', objects.League.NBA),
      ('nfl', objects.League.NFL),
      ('nhl', objects.League.NHL),
      ('nas', objects.League.NASCAR),
      ('soc', objects.League.SOCCER),
      ('golf', objects.League.GOLF),
      ('cfl', objects.League.CFL),
      ('lol', objects.League.LOL),
  ])
  def test_league_to_str(self, expected, value):
    self.assertEqual(expected, enum_convert.league_to_str(value))

  @parameterized.expand([
      (None,),
      ('NBA_CLASSC',),
  ])
  def test_league_to_str_error(self, value):
    with self.assertRaises(Exception):
      enum_convert.league_to_str(value)
