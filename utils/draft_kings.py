"""
Common utils for interacting with the draft kings website.
"""

from objects import ttypes as objects


class InvalidPositionString(Exception):
  """Raised when we can't get the position of a player based on the supplied string and league"""
  pass


def position_from_str(pos, league):
  """Converts a position string into a league.

  Args:
    pos (str): String representation of a position.
    league (objects.League): League to convert for.

  Returns:
    (int) Integer representation of position.
  """
  lea = objects.League
  mapping = {
      (lea.NBA, 'PG'): 0,
      (lea.NBA, 'SG'): 1,
      (lea.NBA, 'SF'): 2,
      (lea.NBA, 'PF'): 3,
      (lea.NBA, 'C'): 4,
  }
  if (league, pos) in mapping.keys():
    return mapping[(league, pos)]

  raise InvalidPositionString('Could not determine the position for league: %s position: %s' %
                              (league, pos))
