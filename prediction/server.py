"""
Entry point for the prediction service.
"""

from prediction_service import Predictor
from prediction import simple_handler
from utils import thrift_util, config_util


if __name__ == '__main__':
  thrift_util.start_server(simple_handler.SimplePredictionHandler(),
                           config_util.get_service_configs('test')['prediction'],
                           Predictor)
