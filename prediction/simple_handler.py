#pylint: disable=no-self-use
"""
Handler for the prediction service that returns dummy information mostly used for testing and
initial setup.
"""

from objects import ttypes as objects


class SimplePredictionHandler(object):
  """Defines the functions needed to handle the prediction_service api, but returns dummy info."""

  def predict_performances(self, athletes, score_type):
    """Returns fake performances for each athlete.

    Args:
      athletes ([objects.Athlete]): List of athlete names to return performances for.
      score_type ([objects.ScoreType]): Scoring method for the user given predicted stats.

    Returns:
      [objects.ActiveAthlete] A list of objects.nba_performance, dummy info in this case.
    """
    del score_type
    active_athletes = []
    for athlete in athletes:
      active_athlete = objects.ActiveAthlete(athlete, 100, 12.0)
      active_athletes.append(active_athlete)
    return active_athletes

  def ping(self):
    """Handles the ping call, simply returns true.

    Returns:
     (bool) Always returns true.
    """
    return True
