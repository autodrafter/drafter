# pylint: disable=missing-docstring
"""
Tests for simple_handler.
"""

import unittest
from parameterized import parameterized

from objects import ttypes as objects
from prediction.simple_handler import SimplePredictionHandler


class TestSimplePredictionHandler(unittest.TestCase):
  """Tests for SimplePredictionHandler."""

  def test_simple_construction(self):
    self.assertIsNotNone(SimplePredictionHandler())

  @parameterized.expand([
      ([],),
      (['jeff'],),
      (['jim', 'bob', 'jeff'],),
  ])
  def test_predict_performances(self, athlete_names):
    athletes = [objects.Athlete(full_name=athlete, league=objects.League.NBA)
                for athlete in athlete_names]
    expected = [objects.ActiveAthlete(athlete, 100, 12.0) for athlete in athletes]

    result = SimplePredictionHandler().predict_performances(athletes,
                                                            objects.ScoreType.CLASSIC)

    self.assertEqual(expected, result)

  def test_ping(self):
    self.assertTrue(SimplePredictionHandler().ping())
