"""
Configuration file for the doit build system.
"""
import itertools

from os import listdir
from os.path import isfile, join, dirname, realpath, basename, splitext

PROJ_DIR = dirname(realpath(__file__))
DOCK_DIR = join(PROJ_DIR, 'docker')
THRIFT_DIR = join(PROJ_DIR, 'thrift')

DOIT_CONFIG = {'default_tasks': ['gen_thrift', 'test'],
               'verbosity': 2}

SERVICES = ['collection', 'prediction', 'lineup', 'draft_kings']
CLIENTS = ['client']
PACKAGES = ['utils']


def get_files_containing(directory, containing):
  """Gets the full paths for all files in a directory containing the str 'containing'"""
  return [join(directory, f) for f in listdir(directory)
          if isfile(join(directory, f)) and containing in f]


def task_docker_build():
  """Builds all the appropriate docker images."""
  docker_files = get_files_containing(DOCK_DIR, 'Dockerfile')
  for d_file in docker_files:
    tag = d_file.split(':')[-1]
    yield {
        'name': tag,
        'actions': ['docker build -t bartushk/autodrafter:%s -f %s %s' % (tag, d_file, DOCK_DIR)],
    }


def task_gen_thrift():
  """Generates thrift output."""
  thrift_files = get_files_containing(THRIFT_DIR, 'thrift')
  output_dir = join(PROJ_DIR, 'genpy')
  for thrift_file in thrift_files:
    thrift_file_name, _ = splitext(basename(thrift_file))
    target = join(output_dir, thrift_file_name)
    yield {
        'name': thrift_file_name,
        'actions': ['mkdir -p %s' % output_dir,
                    'thrift -gen py -out %s %s' % (output_dir, thrift_file)],
        'targets': [target],
    }


def task_start_service():
  """Tasks for running a service from SERVICES"""
  for service in SERVICES:
    service_source = join(PROJ_DIR, service, 'server.py')
    yield {
        'name': service,
        'actions': ['python ' + service_source],
    }


def task_test():
  """Task for running tests."""
  return {
      'actions': ['nosetests -v']
  }


def task_coverage():
  """Task for running test coverage."""
  coverage_string = ''
  for package in SERVICES + CLIENTS + PACKAGES:
    coverage_string += '--cover-package %s ' % package
  return {
      'actions': ['nosetests -v --with-coverage %s' % coverage_string]
  }


def task_lint():
  """Task for running the linter, excludes tests."""
  command = "pylint "
  for directory, sub_dir in itertools.product(SERVICES + CLIENTS + PACKAGES, ['', 'tests/*']):
    command += '%s/%s ' % (directory, sub_dir)
  return {
      'actions': [command]
  }
